# Social Network

** Features: **

+ registration  
+ authentication  
+ display profile  
+ edit profile  
+ ajax search with autocomplete  
+ ajax search with pagination  
+ upload avatar  
+ user export/import to xml  
+ friendships  
+ creation and editing communities  
+ community memberships  
+ account/community posts  
+ personal messages with webSocket chat

** Tools: **  
JDK 8, Spring 5(Boot, Data, Security, Web), JPA 2/Hibernate 5, JMS/ActiveMQ, Jackson, jQuery, StompJS, Mustache Templates, Twitter Bootstrap 4, JUnit 5, Mockito, Maven 3, Slf4j, Log4j, Git/Bitbucket, Tomcat 9, ClearDB/H2DB/MongoDB, Heroku Cloud, IntelliJIDEA
  

** Screenshots **
![Login page](screenshots/login.png)
![Home page](screenshots/profile.png)
![Edit profile](screenshots/edit.png)
![Chat page](screenshots/chat.png)
![Friends](screenshots/friends.png)  
![Members](screenshots/members.png)  

App's heroku link: https://okhanzhin-network.herokuapp.com/  
login: test@mail.ru  
pass: test-pass   
_  
**Khanzhin Oleg**  
Training getJavaJob  
http://www.getjavajob.com  